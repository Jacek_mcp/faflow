var ex = {
	topPos: $('#ExPage > header').outerHeight(),
	headerH: $('body>header nav').outerHeight(),

	scrolled: function() {
	    var scrollPos = $(window).scrollTop();

		// stick aside
		if( scrollPos >= ex.topPos ) {
			if( scrollPos >= $('body').outerHeight() - $('#ExPage aside').outerHeight() - 160 - $('body > footer').outerHeight() ) {
				$('#ExPage aside').removeClass('Stick').addClass('ToBottom');
			}
			else {
				$('#ExPage aside').removeClass('ToBottom').addClass('Stick');
			}
		}
		else {
			$('#ExPage aside').removeClass('Stick').removeClass('ToBottom');
		}
	},

	initScroll: function() {
		$(window).scroll(function() {
		    ex.scrolled();
		});
	},

	updateURL: function(slug){
		window.history.pushState('', '', 'cwiczenia/'+slug);
	},

	exercises: function() {
		if ($(document).width() > 767) {
			$('.ExList li').on('click', function(){
				var active = $(this).attr('data-article');
				$(this).addClass('Active').siblings().removeClass('Active');
				$('#ExPage article[data-id='+active+']').addClass('Show').siblings().removeClass('Show');
				$('html, body').animate({scrollTop: ex.topPos }, 600);
				ex.updateURL(active);
				setTimeout(function(){ 
					ex.bottomPos = $('body > footer').offset().top; 
				}, 500);
				
			});
		} else {
			$('#ExPage article h2').each(function(){ 
				$(this).insertBefore($(this).closest('section'));
			});

			$('#ExPage article h2').on('click', function(){
				$(this).parent().toggleClass('Show').siblings().removeClass('Show');
				var slug = $(this).parent().attr('data-id');
				setTimeout(function(){
					var current = $('#ExPage article.Show')
					if(current.length) {
						setTimeout(function(){
							$('html, body').animate({scrollTop: current.offset().top-ex.headerH }, 400);
						}, 400);
						ex.updateURL(slug);
					} 
					else {
						setTimeout(function(){
							$('html, body').animate({scrollTop: $('#ExPage article:first-of-type').offset().top-ex.headerH }, 400);
						}, 400);
					}
					
				}, 200);
				
			});
		}
	},

	parseUrl: function() {
        var article = window.location.href.split("/").pop(),
        row = '';
        if(article){
        	row = $('.ExList li[data-article='+article+']');
        }

        if(row.length) {
        	row.addClass('Active').siblings().removeClass('Active');
        	var current = $('#ExPage article[data-id='+article+']');
        	current.addClass('Show').siblings().removeClass('Show');
        	$('html, body').animate({scrollTop: current.offset().top - ex.headerH }, 400);
        }
        else {
        	$('.ExList li:first-of-type').addClass('Active').siblings().removeClass('Active');
        	$('#ExPage article:first-of-type').addClass('Show').siblings().removeClass('Show');

        }
    },

	init: function () {
		ex.exercises();
		ex.parseUrl();
		ex.initScroll();
	}
};
