var moments = {

	sliders: function () {
		if ($('#BackstageSlider .swiper-slide').length > 1) {
			var backstageSlider = new Swiper ('#BackstageSlider', {
				direction: 'horizontal',
				autoplay: 10000,
				speed: 2000,
				slidesPerView: 'auto',
				nextButton: '#NextBackstage',
		    	prevButton: '#PrevBackstage',
		    	pagination: '#BackstagePagination',
		    	paginationClickable: 'true',
		    	loop: true
			});
		}
		if ($('#StoriesSlider .swiper-slide').length > 1) {
			var storiesSlider = new Swiper ('#StoriesSlider', {
				direction: 'horizontal',
				autoplay: 10000,
				speed: 2000,
				slidesPerView: 'auto',
				nextButton: '#NextStory',
	        	prevButton: '#PrevStory',
	        	pagination: '#StoriesPagination',
	        	paginationClickable: 'true',
	        	loop: true
			});
		}
    },


	init: function () {
		moments.sliders();
		common.videoModal();

		if ($(document).width() > 767) {
		}
		else {
		}

	}
};
