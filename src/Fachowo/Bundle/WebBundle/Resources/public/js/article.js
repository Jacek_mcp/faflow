var article = {

	mobile: function() {
			$('#ArticlePage h1').prependTo('#ArticlePage > article > header');
	},

	init: function () {

		if ($(document).width() < 768) {
			article.mobile();
		};
	}
};
