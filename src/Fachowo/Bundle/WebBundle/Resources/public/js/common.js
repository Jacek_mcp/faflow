var common = {
	videoId: '',

	cookie: function() {
		if (!localStorage.cookie) {
	        $( "body" ).prepend( "<section id='Cookie' class='Hide'><div class='Body'><p>Serwis wykorzystuje pliki Cookies w celu zapewnienia funkcjonalności strony. Aby dowiedzieć się więcej przejdź do <a href='/polityka-cookies'>Polityki cookies</a>. W każdej chwili możesz zmienić ustawienia swojej przeglądarki internetowej, aby otrzymywać powiadomienia w momencie zapisywania plików cookie lub całkowicie zablokować korzystanie z nich przez przeglądarkę. <button id='cookieClose'>Zamknij</button></p></div></section>" ); 
	    }
	    $('#cookieClose').on('click', function(e){
	        e.preventDefault();
	        localStorage.cookie = 'COOKIE';
	        $('#Cookie').addClass('Hide');
	        setTimeout( function() {
	            $('#Cookie').remove()
	        },300);
	    });

	    setTimeout( function() {
	    	$('#Cookie').removeClass('Hide');
	    },300);
	},

	mobileHeader: function() {
	    $('#Hamburger').on('click', function(e) {  
	        $('body > header nav').toggleClass('ShowMenu');
	    });
	},

    mobileFooter: function() {
	    $('body > footer nav h3:not(.Direct)').on('click', function(e) {  
	        e.preventDefault();
	        $(this).parent().toggleClass('Show').siblings().removeClass('Show');
	    });
	},

	videoPause: function (iframe) {
      $(iframe)[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*'); 
    },

    videoPlay: function (iframe) {
      $(iframe)[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*'); 
    },

    videoModal: function () {
    	$('.VideoTrigger').on('click', function(){
    		var tmpVid = $(this).attr('data-video');
    		if (tmpVid != common.videoId) {
    			common.videoId = tmpVid;
    			var url = "https://www.youtube.com/embed/"+common.videoId+"?rel=0&amp;controls=1&amp;showinfo=0&amp;enablejsapi=1&amp;version=3&amp;playerapiid=ytplayer";
    			$('#Player').attr("src",url).load( function(){ common.videoPlay('#Player'); } );
    		}
    		else {
    			common.videoPlay('#Player');
    		}

    		setTimeout(function(){
    			$('#VideoModal').addClass('Show');
    		}, 200); 
    	});

    	$('#VideoModal .Cross, #VideoModal .Overlay').on('click', function(){
    		common.videoPause('#Player');
    	});

    	$('.Overlay, #VideoModal .Cross').on('click', function () {
    		$('#VideoModal').removeClass('Show');
    	});
    	$(document).keydown(function(e) {
	        // ESCAPE key pressed
	        if (e.keyCode == 27) {
	        	$('#VideoModal').removeClass('Show');
	        }
	    });
    },

	init: function () {
		common.cookie();

		if ($(document).width() < 768) {
			common.mobileHeader();
			common.mobileFooter();
			$('.Trans').removeClass('Trans');
		}
	}
};
