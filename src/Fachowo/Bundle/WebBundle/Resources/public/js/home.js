var home = {
	headerH: $('body > header').outerHeight(),
	menuH: $('body > header nav').outerHeight(),
	sections: [],
	windowH: $( window ).height(),

	slider: function () {
    	var homepageSlider = new Swiper ('#HomeSlider', {
			direction: 'horizontal',
			autoplay: 6000,
			speed: 2000,
			slidesPerView: 'auto',
			nextButton: '#NextSlide',
        	prevButton: '#PrevSlide',
        	pagination: '#Pagination',
        	paginationClickable: 'true'
		})
    },

    header: function() {
	    $('body > header .Wave').on('click', function(e) {
	        e.preventDefault();
	        var pos = $('#HomePage > section:first-of-type()').offset().top - home.menuH;
	        $('html, body').animate({ scrollTop: pos }, 600);  
	    });
	},

	scrolled: function() {
	    var scrollPos = $(window).scrollTop();

		// stick header
		if( scrollPos >= home.headerH - home.menuH ) {
			$('#HomePage').css('margin-top', home.headerH );
			$('body > header').addClass('Stick');
		} 
		else {
			$('#HomePage').css('margin-top', 0 );
			$('body > header').removeClass('Stick');
			// banner scrolling
	    	$('body > header figure').css({'top':  scrollPos/2 + 'px'});
	    	//menu background fading
	    	var opacity = 0.8 + 0.2*scrollPos/(home.headerH-home.menuH);
	    	$('body > header nav').css({'background-color': 'rgba(41, 64, 125, '+ opacity +')' });
	    	$('body > header .Wave').css({'opacity': opacity});
		}
		
		// highlight dots
		var active=0,
		offset = (home.windowH - home.menuH) / 2;

		while (scrollPos >= home.sections[active] - offset && active < home.sections.length) {
            active++;
        }
        $('#Dots span:nth-child('+ (active) +')').addClass('Active').siblings().removeClass('Active');
	},

	mobileScrolled: function() {
	    var scrollPos = $(window).scrollTop();

		// stick header
		if( scrollPos < home.headerH ) {
			// banner scrolling
	    	$('body > header figure').css({'background-position-y':  scrollPos/2 + 'px'});
		}
	},

	initScroll: function() {
		$(window).scroll(function() {
		    home.scrolled();
		});
	},

	initMobileScroll: function() {
		$(window).scroll(function() {
		    home.mobileScrolled();
		});
	},

	initNavigation: function() {
		setTimeout(function(){
			for(var i=0; i<($('#HomePage > section').length); i++) {
	            home.sections[i] = $('#HomePage > section:nth-of-type('+(i+1)+')').offset().top - home.menuH;
	        };			
		}, 1000);

		//dots
		$('#Dots span').on('click', function(){
			var clicked = $(this).index();
			$(this).addClass('Active');
        	$('html, body').animate({scrollTop: home.sections[clicked] }, 600);
		});
	},

	exercises: function() {
		if ($(document).width() > 767) {
			$('.ExList li').on('click', function(){
				var active = $(this).attr('data-article');
				$(this).addClass('Active').siblings().removeClass('Active');

				var current = $('#Exercises article[data-id='+active+']');
				current.addClass('Show').siblings().removeClass('Show');
				$('html, body').animate({scrollTop: current.offset().top - home.menuH - 40 }, 600);
			});
		} else {
			$('#Exercises article h4').on('click', function(){
				$(this).parent().toggleClass('Show').siblings().removeClass('Show');
				var current = $(this).parent();
				setTimeout(function(){
					$('html, body').animate({scrollTop: current.offset().top - home.menuH }, 400);
				}, 400);
			});
		}
	},

	init: function () {
		home.slider();
		home.header();
		home.exercises();
		

		common.videoModal();

		if ($(document).width() > 767) {
			home.initScroll();
			home.scrolled();
			home.initNavigation();
		}
		else {
			//home.initMobileScroll();
		}

	}
};
