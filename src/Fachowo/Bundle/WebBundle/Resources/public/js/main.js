$(document).ready(function () {
    if ($('#HomePage').length > 0) {
        home.init();
    }
    else if ($('#ExPage').length > 0) {
        ex.init();
    }
    else if ($('#ArticlePage').length > 0) {
        article.init();
    }
    else if ($('#MomentsPage').length > 0) {
        moments.init();
    }

    common.init();
});
