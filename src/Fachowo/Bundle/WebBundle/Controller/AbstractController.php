<?php

namespace Fachowo\Bundle\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Fachowo\Bundle\CoreBundle\Entity\Settings;
use Fachowo\Bundle\CoreBundle\Repository\SettingsRepository;

/**
 * Class AbstractController
 * @package Fachowo\Bundle\WebBundle\Controller
 */
abstract class AbstractController extends Controller
{

}
