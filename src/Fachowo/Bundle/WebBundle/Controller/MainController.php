<?php

namespace Fachowo\Bundle\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class MainController
 * @package Fachowo\Bundle\WebBundle\Controller
 */
class MainController extends AbstractController
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('FachowoWebBundle::layout.html.twig');
    }
}
