<?php

namespace Fachowo\Bundle\WebBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class FachowoWebBundle
 * @package Fachowo\Bundle\WebBundle
 */
class FachowoWebBundle extends Bundle
{
}
