<?php

namespace Fachowo\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('roles', ChoiceType::class, [
                'mapped' => false,
                'required' => true,
                'label'    => 'Uprawnienia',
                'choices'=>[
                    'Admin'=>'ROLE_ADMIN',
                    'Super Ekstra Hiper Mega Admin'=>'ROLE_SUPER_ADMIN',
                    'User'=>'ROLE_USER'
                ],
                'expanded'   => true
            ])
        ;
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fachowo\Bundle\CoreBundle\Entity\User'
        ));
    }
}
