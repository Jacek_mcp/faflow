<?php
namespace Fachowo\Bundle\CoreBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ImageTypeExtension
 * @package Fachowo\Bundle\CoreBundle\Form\Extension
 */
class ImageTypeExtension extends AbstractTypeExtension
{
    /**
     * Returns the name of the type being extended.
     *
     * @return string The name of the type being extended
     */
    public function getExtendedType()
    {
        return FileType::class;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['image']);
        $resolver->setDefined(['name']);
    }

    /**
     * Pass the image URL to the view
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (array_key_exists('image', $options)) {
            $parentData = $form->getParent()->getData();
            if (null !== $parentData) {
                $accessor = PropertyAccess::createPropertyAccessor();
                $imageUrl = $accessor->getValue($parentData, $options['image']);
            } else {
                $imageUrl = null;
            }
            // set an "image_url" variable that will be available when rendering this field
            $view->vars['file_url'] = $imageUrl;
        }
        if (array_key_exists('name', $options)) {
            $parentData = $form->getParent()->getData();
            if (null !== $parentData && !empty($options['name'])) {
                $accessor = PropertyAccess::createPropertyAccessor();
                $imageUrl = $accessor->getValue($parentData, $options['name']);
            } else {
                $imageUrl = null;
            }
            // set an "image_url" variable that will be available when rendering this field
            $view->vars['name'] = $imageUrl;
        }
    }
}
