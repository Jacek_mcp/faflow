<?php

namespace Fachowo\Bundle\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'rozpoczęty'=>'rozpoczęty',
                    'zakończony' => 'zakończony',
                    'nierozpoczęty' => 'nierozpoczęty',
                    'na finiszu' => 'na finiszu'
                ]
            ])
            ->add('client')
            ->add('description', TextType::class)
            ->add('image', FileType::class, [
                'label'=>'plik zdjeciowy',
                'data_class' => null,
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Fachowo\Bundle\CoreBundle\Entity\Project'
        ));
    }
}
