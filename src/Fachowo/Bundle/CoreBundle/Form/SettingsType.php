<?php

namespace Fachowo\Bundle\CoreBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SettingsType
 * @package Fachowo\Bundle\CoreBundle\Form
 */
class SettingsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('test', TextType::class);
//            ->add('stage', 'choice', [
//                'label' => 'Etap',
//                'choices' => [
//                    'form' => 'Przyjmowanie zgłoszeń',
//                    'end-form' => 'Zakończenie przyjmowania zgłoszeń',
//                    'vote' => 'Głosowanie',
//                    'end-vote' => 'Zakończenie głosowania',
//                    'winners' => 'Zwycięzcy'
//                ]
//            ]);
//            ->add('endFormDate', null, ['label' => 'Data zakończenia przyjmowania zgłoszeń'])
//            ->add('startDate', null, ['label' => 'Data rozpoczęcia głosowania'])
//            ->add('finishDate', null, ['label' => 'Data zakończenia głosowania'])
//            ->add('winnerDate', null, ['label' => 'Data ogłoszenia wyników'])
//            ->add('facebookUrl', 'url', ['label' => 'facebook (z https://)', 'required' => false])
//            ->add('twitterUrl', 'url', ['label' => 'twitter  (z https://)', 'required' => false])
//            ->add('ga', null, ['label' => 'Google Analytics ID (UA-XXX-X)'])
//            ->add('email', null, ['label' => 'Email kontaktowy'])
//            ->add('metaTitle', null, ['label' => 'Meta Title'])
//            ->add('metaDescription', null, ['label' => 'Meta Description'])
//            ->add('metaKeywords', null, ['label' => 'Meta Keywords']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Fachowo\Bundle\CoreBundle\Entity\Settings'
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'fachowo_bundle_corebundle_settings';
    }
}
