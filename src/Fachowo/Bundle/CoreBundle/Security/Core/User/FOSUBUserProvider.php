<?php

namespace Fachowo\Bundle\CoreBundle\Security\Core\User;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class FOSUBUserProvider
 * @package Fachowo\Bundle\CoreBundle\Security\Core\User
 */
class FOSUBUserProvider extends BaseClass
{

    /**
     * {@inheritDoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();

        $setter = 'set' . ucfirst($service);
        $setter_id = $setter . 'Id';
        $setter_token = $setter . 'AccessToken';

        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy([$property => $username])) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());

        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $username = $response->getUsername();
        $email = $response->getEmail();

        $user = $this->userManager->findUserBy([$this->getProperty($response) => $username]);

        if (null === $user) {
            $user = $this->userManager->findUserBy(['email' => $email]);
            if (null === $user) {
                $service = $response->getResourceOwner()->getName();
                $setter = 'set' . ucfirst($service);
                $setter_id = $setter . 'Id';
                $setter_token = $setter . 'AccessToken';
                // create new user here
                $user = $this->userManager->createUser();
                $user->$setter_id($username);
                $user->$setter_token($response->getAccessToken());


                if ($response->getResourceOwner()->getName() == 'facebook') {
                    $user->setUsername('fb_' . $username);
                } else {
                    $user->setUsername('tw_' . $username);
                }

                $user->setEmail('');
                $user->setPlainPassword($username.md5(time()));

                $user->setEnabled(true);
                $this->userManager->updateUser($user);
                return $user;

            } else {
                $this->userManager->updateUser($user);
                return $user;
            }
        }

        //if user exists - go with the HWIOAuth way
        $user = parent::loadUserByOAuthUserResponse($response);

        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set' . ucfirst($serviceName) . 'AccessToken';

        //update access token
        $user->$setter($response->getAccessToken());

        return $user;
    }
}
