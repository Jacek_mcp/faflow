<?php

namespace Fachowo\Bundle\CoreBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class FachowoCoreBundle
 * @package Fachowo\Bundle\CoreBundle
 */
class FachowoCoreBundle extends Bundle
{
}
