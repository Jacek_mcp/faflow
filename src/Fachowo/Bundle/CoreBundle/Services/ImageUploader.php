<?php
/**
 * Created by PhpStorm.
 * User: jacek
 * Date: 07.10.16
 * Time: 17:30
 */

namespace Fachowo\Bundle\CoreBundle\Services;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploader
{
    private $targetDir;

    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    public function upload(UploadedFile $file)
    {
        $fileName = 'images/project/'.md5(uniqid()).'.'.$file->guessExtension();
        $file->move($this->targetDir, $fileName);

        return $fileName;
    }
}