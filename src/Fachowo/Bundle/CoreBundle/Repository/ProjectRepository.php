<?php
/**
 * Created by PhpStorm.
 * User: jacek
 * Date: 29.09.16
 * Time: 14:28
 */

namespace Fachowo\Bundle\CoreBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ProjectRepository extends EntityRepository
{
    public function getAllActiveProject($isDelete = false)
    {
        $query = $this->_em->createQuery('SELECT p, c, t, u, y '
            . 'FROM FachowoCoreBundle:Project p '
            . 'LEFT JOIN p.client c '
            . 'LEFT JOIN p.createdBy u '
            . 'LEFT JOIN p.updatedBy y '
            . 'LEFT JOIN p.tasks t '
            . 'WHERE p.isDelete =:isDelete ')
            ->setParameter('isDelete', $isDelete);

        return $query->getArrayResult();
    }
}