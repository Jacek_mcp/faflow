<?php

namespace Fachowo\Bundle\CoreBundle\Twig;

use Symfony\Component\Security\Csrf\CsrfTokenManager;

/**
 * Class CsrfTwigExtension
 * @package Finance\Bundle\CoreBundle\Twig
 */
class CsrfTwigExtension extends \Twig_Extension
{
    /** @var CsrfTokenManager $csrfProvider */
    protected $csrfProvider;

    /**
     * @param $csrfProvider
     */
    public function __construct($csrfProvider)
    {
        $this->csrfProvider = $csrfProvider;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'csrf_twig_extension';
    }

    /**
     * Returns a list of functions to add to the existing list.
     *
     * @return array An array of functions
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('default_csrf_token', [$this, 'getCsrfToken'])
        ];
    }

    /**
     * @return mixed
     */
    public function getCsrfToken()
    {
        return $this->csrfProvider->getToken('default');
    }
}
