<?php

namespace Fachowo\Bundle\CoreBundle\Twig;

/**
 * Class FachowoExtension
 *
 * @package Fachowo\Bundle\CoreBundle\Twig
 */
class FachowoExtension extends \Twig_Extension
{

    /**
     * Return the functions registered as twig extensions
     *
     * @return array
     */
    public function getFunctions()
    {
        return [];
    }

    /**
     * Get filters
     *
     * @return array
     */
    public function getFilters()
    {
        return [];
    }

    /**
     * Get Extension Name
     *
     * @return string
     */
    public function getName()
    {
        return 'fachowo_extension';
    }
}
