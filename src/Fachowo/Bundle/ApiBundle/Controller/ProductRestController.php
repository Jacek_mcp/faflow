<?php

//https://codereviewvideos.com/course/symfony-3-with-reactjs-and-angular/video/api-get-a-collection-of-blogposts

namespace Fachowo\Bundle\ApiBundle\Controller;

use Fachowo\Bundle\ApiBundle\Entity\Product;

use Fachowo\Bundle\ApiBundle\Form\ProductType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



/**
 * Class ProductRestController
 * @package Fachowo\Bundle\ApiBundle\Controller
 */
class ProductRestController extends FOSRestController implements ClassResourceInterface
{
    /**
     * @param $id
     * @return Response
     *
     * @Rest\Get("/get/product/{id}")
     * @ApiDoc(
     *     output="ApiBundle\Entity\Product",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function getAction($id)
    {
        $product = $this->getDoctrine()->getRepository("FachowoApiBundle:Product")->find($id);
        if (!$product) {
            return new JsonResponse(null, 404);
        }
        return new JsonResponse(['product'=>$product], 200);
    }

    /**
     * @param $request
     * @return Response
     *
     * @Rest\Post("/post/product")
     * @ApiDoc(
     *     input="ApiBundle\Form\ProductType",
     *     output="ApiBundle\Entity\Product",
     *     statusCodes={
     *         201 = "Returned when a new post has been successful created",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(ProductType::class, null, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }
        /**
         * @var $product Product
         */
        $product = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();

        $routeOptions = [
            'id' => $product->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_product_rest', $routeOptions, Response::HTTP_CREATED);

    }

}