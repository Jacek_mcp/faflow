<?php

namespace Fachowo\Bundle\AdminBundle\Controller;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Fachowo\Bundle\CoreBundle\Entity\Logger;

/**
 * Logger controller.
 *
 */
class LoggerController extends Controller
{
    /**
     * Lists all Logger entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $loggers = $em->getRepository('FachowoCoreBundle:Logger')->findAll();

        return $this->render('FachowoAdminBundle:logger:index.html.twig', array(
            'loggers' => $loggers,
        ));
    }

    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $logger = $em->getRepository('FachowoCoreBundle:Logger')->find($id);

        $tableName = $logger->getTableName();

        return $this->render("FachowoAdminBundle:".$tableName.":show.html.twig", [
            'loggerId'=>$logger->getId(),
            $tableName=>$logger->getChanges(),
            'logger'=>true,
        ]);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function restoreAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $logger = $em->getRepository('FachowoCoreBundle:Logger')->find($id);

        $rowId = $logger->getRowId();
        $tableName = $logger->getTableName();
        $repoName = ucfirst($tableName);

        $entity = $em->getRepository('FachowoCoreBundle:'.$repoName)->find($rowId);

        $entity->setIsDelete(false);

        $em->persist($entity);
        $em->flush();

        return $this->redirectToRoute('logger_index');
    }
}
