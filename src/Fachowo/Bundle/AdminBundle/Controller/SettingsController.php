<?php

namespace Fachowo\Bundle\AdminBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

use Fachowo\Bundle\CoreBundle\Entity\Settings;
use Fachowo\Bundle\CoreBundle\Form\SettingsType;

/**
 * Settings controller.
 *
 */
class SettingsController extends AbstractController
{
    /**
     * Displays a form to edit an existing Settings entity.
     *
     */
    public function editAction()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Settings $entity */
        $entity = $em->getRepository('FachowoCoreBundle:Settings')->findOneBy([]);

        if (!$entity) {
            $entity = new Settings();
            $em->persist($entity);
            $em->flush();
        }

        $editForm = $this->createEditForm($entity);

        $this->breadcrumbs([['Ustawienia']]);

        return $this->render('FachowoAdminBundle:Settings:edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        ]);
    }

    /**
     * Creates a form to edit a Settings entity.
     *
     * @param Settings $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Settings $entity)
    {
        $form = $this->createForm(SettingsType::class, $entity, [
            'action' => $this->generateUrl('admin_settings_update'),
            'method' => 'PUT',
        ]);

        $form->add('submit', SubmitType::class, [
            'label' => 'Zapisz', 'attr' => ['class' => 'btn btn-s-md btn-success']]);

        return $form;
    }

    /**
     * Edits an existing Settings entity.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Settings $entity */
        $entity = $em->getRepository('FachowoCoreBundle:Settings')->findOneBy([]);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('notice', 'Pozycja została zapisana.');

            return $this->redirect($this->generateUrl('admin_settings_edit'));
        }

        $this->breadcrumbs([['Ustawienia']]);

        return $this->render('FachowoAdminBundle:Settings:edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        ]);
    }
}
