<?php

namespace Fachowo\Bundle\AdminBundle\Controller;

use Fachowo\Bundle\CoreBundle\Repository\TaskRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Fachowo\Bundle\CoreBundle\Entity\Task;
use Fachowo\Bundle\CoreBundle\Form\TaskType;


class TaskController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tasks = $em->getRepository('FachowoCoreBundle:Task')->getAllActiveTask();

        return $this->render('FachowoAdminBundle:task:index.html.twig', array(
            'tasks' => $tasks,
        ));
    }

    public function newAction(Request $request)
    {
        $task = new Task();

        $form = $this->createForm('Fachowo\Bundle\CoreBundle\Form\TaskType', $task);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);

            $em->flush();

            return $this->redirectToRoute('task_show', array('id' => $task->getId()));
        }


        return $this->render('FachowoAdminBundle:task:new.html.twig', array(
            'task' => $task,
            'form' => $form->createView(),
        ));
    }

    public function showAction(Task $task)
    {
        $deleteForm = $this->createDeleteForm($task);

        return $this->render('FachowoAdminBundle:task:show.html.twig', array(
            'task' => $task,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function editAction(Request $request, Task $task)
    {
        $deleteForm = $this->createDeleteForm($task);
        $editForm = $this->createForm('Fachowo\Bundle\CoreBundle\Form\TaskType', $task);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            return $this->redirectToRoute('task_edit', array('id' => $task->getId()));
        }

        return $this->render('FachowoAdminBundle:task:edit.html.twig', array(
            'task' => $task,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction(Request $request, Task $task)
    {
        $form = $this->createDeleteForm($task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $task->setIsDelete(true);
            $em->persist($task);
            $em->flush();
        }

        return $this->redirectToRoute('task_index');
    }

    private function createDeleteForm(Task $task)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('task_delete', array('id' => $task->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function archiveAction()
    {
        $em = $this->getDoctrine()->getManager();

        /** @var TaskRepository $taskRepo */
        $taskRepo = $em->getRepository('FachowoCoreBundle:Task');
        $tasks = $taskRepo->getAllTaskByStatus();

        return $this->render('FachowoAdminBundle:task:archive.html.twig', [
            'tasks' => $tasks
        ]);
    }
}
