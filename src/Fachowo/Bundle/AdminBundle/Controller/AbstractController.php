<?php

namespace Fachowo\Bundle\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class AbstractController
 * @package Fachowo\Bundle\AdminBundle\Controller
 */
abstract class AbstractController extends Controller
{
    /**
     * Before load
     */
    public function beforeLoad()
    {
    }

    /**
     * @param bool|false $routes
     */
    public function breadcrumbs($routes = true)
    {
        $breadcrumbs = $this->get("white_october_breadcrumbs");
        $breadcrumbs
            ->addItem('<i class="fa fa-home"></i> Home', $this->get("router")->generate("fachowo_admin_dashboard"));
        if ($routes) {
            /** @var array $routes */
            foreach ($routes as $route) {
                if (isset($route[1])) {
                    $breadcrumbs->addItem($route[0], $this->get("router")->generate($route[1]));
                } else {
                    $breadcrumbs->addItem($route[0]);
                }

            }
        }
    }
}
