<?php



namespace Fachowo\Bundle\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Fachowo\Bundle\CoreBundle\Entity\Project;
use Fachowo\Bundle\CoreBundle\Form\ProjectType;

/**
 * Project controller.
 *
 */
class ProjectController extends AbstractController
{
    /**
     * Lists all Project entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projects = $em->getRepository('FachowoCoreBundle:Project')->getAllActiveProject();
        $this->breadcrumbs([['Projekty']]);

        return $this->render('FachowoAdminBundle:project:index.html.twig', array(
            'projects' => $projects,
        ));
    }

    /**
     * Creates a new Project entity.
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm('Fachowo\Bundle\CoreBundle\Form\ProjectType', $project);
        $form->handleRequest($request);
        $this->breadcrumbs([['Projekty', 'project_index'],['Nowy']]);

        if ($form->isSubmitted() && $form->isValid()) {
            $image = $project->getImage();
            $imageName = $this->get('admin.image_uploader')->upload($image);
            $project->setImage($imageName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('project_show', array('id' => $project->getId()));
        }

        return $this->render('FachowoAdminBundle:project:new.html.twig', array(
            'project' => $project,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Project entity.
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Project $project)
    {
        $em = $this->getDoctrine()->getManager();

        $deleteForm = $this->createDeleteForm($project);
        $this->breadcrumbs([['Projekty', 'project_index'],[$project->getName()]]);

        return $this->render('FachowoAdminBundle:project:show.html.twig', array(
            'project' => $project,
            'delete_form' => $deleteForm->createView(),
            'id' => $project->getId()
        ));
    }

    /**
     * Displays a form to edit an existing Project entity.
     * @param Request $request
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Project $project)
    {
        $deleteForm = $this->createDeleteForm($project);
        $editForm = $this->createForm('Fachowo\Bundle\CoreBundle\Form\ProjectType', $project);
        $editForm->handleRequest($request);
        $this->breadcrumbs([['Projekty', 'project_index'],[' / edycja'],[$project->getName()]]);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $image = $project->getImage();
            $imageName = $this->get('admin.image_uploader')->upload($image);
            $project->setImage($imageName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('project_edit', array('id' => $project->getId()));
        }

        return $this->render('FachowoAdminBundle:project:edit.html.twig', array(
            'project' => $project,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Project entity.
     * @param Request $request
     * @param Project $project
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Project $project)
    {
        $form = $this->createDeleteForm($project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $project->setIsDelete(true);
            $em->persist($project);
            $em->flush();
        }

        return $this->redirectToRoute('project_index');
    }

    /**
     * Creates a form to delete a Project entity.
     *
     * @param Project $project The Project entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Project $project)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('project_delete', array('id' => $project->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
