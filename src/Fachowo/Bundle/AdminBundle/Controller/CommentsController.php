<?php

namespace Fachowo\Bundle\AdminBundle\Controller;

use Fachowo\Bundle\CoreBundle\Repository\CommentsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Fachowo\Bundle\CoreBundle\Entity\Comments;

/**
 * Comments controller.
 *
 */
class CommentsController extends Controller
{
    /**
     * Lists all Comments entities.
     * @param $model
     * @param $modelId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($model, $modelId)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var CommentsRepository $commentsRepo */
        $commentsRepo = $em->getRepository('FachowoCoreBundle:Comments');
        $comments = $commentsRepo->getAllByCategoryAndId($modelId, $model);

        return $this->render('FachowoAdminBundle:comments:index.html.twig', array(
            'comments' => $comments,
        ));
    }

    /**
     * Creates a new Comments entity.
     * @param Request $request
     * @param $model
     * @param $modelId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, $model, $modelId)
    {


        $comment = new Comments();
        $comment->setModel($model)
            ->setModelId($modelId);
        $form = $this->createForm('Fachowo\Bundle\CoreBundle\Form\CommentsType', $comment, [
            'action' => $this->generateUrl('comments_new', ['model'=>$model, 'modelId'=>$modelId]),
            'method' => 'POST',
            'attr'=>['class'=>'form-inline']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute($model.'_show', array('id' => $modelId));
        }

        return $this->render('FachowoAdminBundle:comments:new.html.twig', array(
            'routing' => $comment,
            'form' => $form->createView(),
        ));
    }

//    /**
//     * Displays a form to edit an existing Comments entity.
//     * @param Request $request
//     * @param Comments $routing
//     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
//     */
//    public function editAction(Request $request, Comments $routing)
//    {
//        $deleteForm = $this->createDeleteForm($routing);
//        $editForm = $this->createForm('Fachowo\Bundle\CoreBundle\Form\CommentsType', $routing);
//        $editForm->handleRequest($request);
//
//        if ($editForm->isSubmitted() && $editForm->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($routing);
//            $em->flush();
//
//            return $this->redirectToRoute('comments_edit', array('id' => $routing->getId()));
//        }
//
//        return $this->render('comments/edit.html.twig', array(
//            'routing' => $routing,
//            'edit_form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
//        ));
//    }

    /**
     * Deletes a Comments entity.
     * @param Request $request
     * @param Comments $comment
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Comments $comment)
    {
        $form = $this->createDeleteForm($comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();
        }

        return $this->redirectToRoute('comments_index');
    }

    /**
     * Creates a form to delete a Comments entity.
     *
     * @param Comments $comment The Comments entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Comments $comment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comments_delete', array('id' => $comment->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

}
