<?php

namespace Fachowo\Bundle\AdminBundle\Controller;

/**
 * Class MainController
 * @package Fachowo\Bundle\AdminBundle\Controller
 */
class MainController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction()
    {
        return $this->render('FachowoAdminBundle:Main:index.html.twig');
    }
}
