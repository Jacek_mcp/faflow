<?php

namespace Fachowo\Bundle\AdminBundle\EventListener;

use Symfony\Bundle\WebProfilerBundle\Controller\ExceptionController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Fachowo\Bundle\AdminBundle\Controller\AbstractController;

/**
 * Class BeforeControllerListener
 * @package Fachowo\Bundle\AdminBundle\EventListener
 */
class BeforeControllerListener
{

    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            // not a object but a different kind of callable. Do nothing
            return;
        }

        $controllerObject = $controller[0];

        // skip initializing for exceptions
        if ($controllerObject instanceof ExceptionController) {
            return;
        }

        if ($controllerObject instanceof AbstractController) {
            // this method is the one that is part of the interface.
            $controllerObject->beforeLoad();
        }
    }
}
