<?php

namespace Fachowo\Bundle\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FachowoAdminBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}