<?php
/**
 * Created by PhpStorm.
 * User: jacek
 * Date: 24.10.16
 * Time: 18:48
 */

namespace Fachowo\Bundle\AdminBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Fachowo\Bundle\ApiBundle\Entity\Product;
use Fachowo\Bundle\CoreBundle\Entity\Comments;
use Fachowo\Bundle\CoreBundle\Entity\Logger;
use Fachowo\Bundle\CoreBundle\Entity\User;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class LoggerSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
            'preRemove',
        );
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $type = "Create new";
        $this->index($args, $type);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $type="Update";
        $this->index($args, $type);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $type = "Remove";
        $this->index($args, $type);
    }

    /**
     * @param LifecycleEventArgs $args
     * @param $action
     */
    public function index(LifecycleEventArgs $args, $action)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Logger and !$entity instanceof User and !$entity instanceof Comments and !$entity instanceof Product) {

            $entity->getIsDelete() == true ? $action = "remove": $action = $action ;

            $logger = new Logger();

            $em = $args->getEntityManager();

            $tableName = $em->getClassMetadata(get_class($entity))->getTableName();
            $rowId = $entity->getId();

            $logger->setAction($action);
            $logger->setRowId($rowId);
            $logger->setTableName($tableName);
            $logger->setChanges($entity);

            $em->persist($logger);
            $em->flush();
        }
    }
}
//            $repoName = $em->getClassMetadata(get_class($entity))->getName();
//            $checker = $em->getRepository($repoName)->findAll();

//            $serializer = SerializerBuilder::create()->build();
//            $jsonChanges = $serializer->serialize($entity, 'json',
//                SerializationContext::create()->enableMaxDepthChecks());